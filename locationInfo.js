var x=document.getElementById("demo");
function getLocation()
{
	if(navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(showPosition);
	}
	else
	{
		x.innerHTML="Geolocation is not supported by this browser.";
	}
}
function showPosition(position)
{
	var latitude=position.coords.latitude;
	localStorage.setItem("latitude",position.coords.latitude);
	var longitude=position.coords.longitude;
	localStorage.setItem("longitude",position.coords.longitude);
	var key='b1c33088691da991f32b5e4d54ed03f6';
	document.getElementById("showMetheForecast").innerHTML="";
	$.ajax(
	{
		url:'https://api.openweathermap.org/data/2.5/weather',
		dataType:'json',
		type:'GET',
		data:{lat:latitude,lon:longitude,appid:key,units:'metric'},
		success:function(data)
		{
			$.each(data.weather, function(index,val)
			{
				//document.getElementById("showMetheForecast").innerHTML='<?xml version="1.0" encoding="UTF-8"?><weather><name>'+data.name+'</name><icon>http://openweathermap.org/img/w/'+val.icon+'.jpg</icon><temperature>'+data.main.temp+' C</temperature><status>'+val.main.toUpperCase()+' ('+val.description.toUpperCase()+')</status></weather>';

				localStorage.setItem("all_values",'<table border="1"><tr align="center" class="header"><td>Location</td><td>Weather Status</td><td>Temperature</td></tr><tr align="center"><td class="nameVal"><b>'+data.name+'</b></td><td class="statusVal"><img src="http://openweathermap.org/img/w/'+val.icon+'.png"/><br>'+val.main.toUpperCase()+' ('+val.description.toUpperCase()+')</td><td class="tempVal">'+data.main.temp+'&deg; C'+'</td></tr></table>');
			});
		}
	});
	document.getElementById("showMetheForecast").innerHTML=localStorage.getItem("all_values");
	//loadXMLDoc(localStorage.getItem("all_values"));
}
/*
function loadXMLDoc(XMLData)
{
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      showXMLData(this);
    }
  };
  xmlhttp.open("GET",, true);
  xmlhttp.send();
}
function showXMLData(xml)
{
  var i;
  var xmlDoc = xml.responseXML;
  var table="<tr><th>Location</th><th>Status</th></tr>";
  var x=xmlDoc.getElementsByTagName("name");
  for(i=0;i<x.length;i++)
  { 
    table+="<tr><td>"+
    x[i].getElementsByTagName("name")[0].childNodes[0].nodeValue +
    "</td><td>" +
    x[i].getElementsByTagName("icon")[0].childNodes[0].nodeValue +
    "</td></tr>";
    x[i].getElementsByTagName("temperature")[0].childNodes[0].nodeValue +
    "</td></tr>";
    x[i].getElementsByTagName("status")[0].childNodes[0].nodeValue +
    "</td></tr>";
  }
  document.getElementById("demo").innerHTML = table;
}
*/