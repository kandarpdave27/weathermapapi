Author: Kandarp Dave
Test Starting Date and Time: 23-04-2019 4:30 PM
Test Starting Date and Time: 23-04-2019 2:00 PM

How to run program:

1. Download all the files.
2. I have made .html, .css, .js files in the folder which can run client (no server needed - Stable internet connection required).
3. Open index.html file using google chrome.
4. With page loading, it will ask you give permission to access location. So please select Allow.
5. Make sure in chrome settings, you have enable the option for asking location permission.

Note:

1. Visual Studio is not installed in the home computer so Author could not completed task with ReactJS.